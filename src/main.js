import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import store from './store'
import router from './router'

Vue.use(VueRouter)
var http = axios.create({
  baseURL: (process.env.NODE_ENV === 'development' ? 'http://localhost:8082' : 'http://fayo.bolow.me'),
  timeout: 15 * 1000,
  headers: {
      'Content-Type': 'application/json'
  }
})
http.interceptors.request.use(config => {
  let auth = window.localStorage.getItem('auth')
  if (auth !== null) {
    auth = JSON.parse(auth)
    config.headers['Authorization'] = auth.jwt_token
  }
  return config
}, err => {
  console.log('axiso error', err)
})
Vue.prototype.$http = http
Vue.config.productionTip = false

export const EventBus = new Vue()

export default new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
