const state = {
  drawer: true,
  layout: true
}

const getters = {
  drawer: (state) => state.drawer,
  layout: (state) => state.layout
}

const mutations = {
  DRAWER (state, value) {
    state.drawer = value === true
  }
}

const actions = {
  CLEAR_ALL (context) {
    context.commit('CLEAR_USER_DATA')
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}