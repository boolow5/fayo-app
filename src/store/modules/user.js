import Vue from '../../main'

export const isLoggedIn = function () {
  console.log('isLoggedIn', {length: state.jwt_token.length, str: state.jwt_token, isTrue: state && state.jwt_token && state.jwt_token.length > 50})
  return state && state.jwt_token && state.jwt_token.length > 50
}

const state = {
  jwt_token: '',
  level: null,
  profile: {}
}

const getters = {
  is_logged_in: (state) => state.jwt_token && state.jwt_token.length > 50,
  profile: (state) => state.profile
}

const mutations = {
  SET_TOKEN (state, token) {
    console.log('SET_TOKEN', token)
    state.jwt_token = token
  },
  SET_LEVEL (state, level) {
    state.level = level
  },
  SET_PROFILE (state, profile) {
    state.profile = profile
  },
  CLEAR_USER_DATA (state) {
    state.jwt_token = ''
    state.level = null
    state.profile = {}
  }
}

const actions = {
  LOGIN (context, data) {
    console.log('LOGIN action', data)
    return new Promise((resolve, reject) => {
      Vue.$http.post('/api/v1/login', data).then(response => {
        console.log('LOGIN response', data.token)
        if (response && response.data && response.data.token) {
          context.commit('SET_TOKEN', response.data.token)
          context.commit('SET_LEVEL', response.data.level)
        }
        resolve(response.data)
      }).catch(err => {
        console.log('LOGIN error', data)
        reject(err)
      })
    })
  },
  SIGNUP (context, data) {
    console.log('SIGNUP action', data)
    return new Promise((resolve, reject) => {
      Vue.$http.post('/api/v1/signup', data).then(response => {
        console.log('SIGNUP response', data.token)
        if (response && response.data && response.data.token) {
          context.commit('SET_TOKEN', response.data.token)
          context.commit('SET_LEVEL', response.data.level)
        }
        resolve(response.data)
      }).catch(err => {
        console.log('LOGIN error', data)
        reject(err)
      })
    })
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}