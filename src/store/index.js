import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import app from './modules/app'
import user from './modules/user'

const localStorage = (store) => {
  store.subscribe((mutation) => {
    window.localStorage.setItem('auth', JSON.stringify(user.state))
    if (mutation.type === 'CLEAR_ALL') {
      window.localStorage.removeItem('auth')
    }
  })
}

export default new Vuex.Store({
  modules: {
    app,
    user
  },
  plugins: [localStorage]
})
