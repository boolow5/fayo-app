import user from '../modules/user'

export default {
  localStoragePlugin (store) {
    store.subscribe((mutation) => {
      window.localStorage.setItem('auth', JSON.stringify(user.state))
      if (mutation.type === 'CLEAR_ALL') {
        window.localStorage.removeItem('auth')
      }
    })
  }
}